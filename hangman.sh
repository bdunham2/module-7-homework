#!/bin/bash

#assign variable for file name
wordList="hangmanWordList.txt"
empty="\."
games=0

#error statements for if file is not found
if [ ! -r "$wordList" ] ;then
 echo "Word list: $wordList is missing. Cannot start game" >&2
 exit 1
fi

#intro statement saying game has started and how to quit
echo "*Starting new game!*"
echo "Type 'quit' to quit at any time"

#while loop that starts the game
while [ "$userGuess" != "quit" ] ; do
 #selects random word from the file
 correctWord="$(shuf -n1 $wordList)"
 #new game statement for whenever loop is repeated
 if [ $games -gt 0 ]
 then echo ""
 echo "*Starting New Game*"
 fi
 games="$(( $games + 1 ))"
 guessedLetters="" ; userGuess="" ; attempts=${1:-6}
 wordStatus="$(echo $correctWord | sed "s/[^$empty${guessedLetters}]/-/g")"

 while [ "$userGuess" != "$correctWord" -a "$userGuess" != "quit" ]
 do echo ""
 #shows user what letters they have guessed
 if [ ! -z "$guessedLetters" ]
 then echo "Letters guessed: $guessedLetters"
 fi
 #tells user how many attempts are remaining and to guess a letter
 echo "Attempts remaining: $attempts"
 echo "Word: $wordStatus"
 echo -n "Guess a letter: "
 read userGuess
 echo ""
 
 #loop will end if user types 'quit' 
if [ "$userGuess" = "quit" ] ; then
 exit 0 
# Reminds user to input single lowercase letters only
elif [ $(echo $userGuess | wc -c | sed 's/[^[:digit:]]//g') -ne 2 ]
 then echo "You can only guess a single letter at a time"
elif [ ! -z "$(echo $userGuess | sed 's/[[:lower:]]//g')" ]
 then echo "You can only guess lowercase letters"
# reminds user that they guessed that letter already
elif [ -z "$(echo $userGuess | sed "s/[$empty$guessedLetters]//g")" ]
 then echo "You have already guessed: $userGuess. Please try a different letter"

# condition to check if guessed letter is in word
 elif [ "$(echo $correctWord | sed "s/$userGuess/-/g")" != "$correctWord" ] ; then
 guessedLetters="$guessedLetters$userGuess"
 wordStatus="$(echo $correctWord | sed "s/[^$empty${guessedLetters}]/-/g")"
 
 #if every letter is guessed, user wins
 if [ "$wordStatus" = "$correctWord" ]
 then echo "Congratulations! You win! :) The word was \"$correctWord\"."
 userGuess="$correctWord" 
 else
 echo "Correct! The letter \"$userGuess\" is in the word!"
 fi

 #if all attempts used, user loses and game ends
 elif [ $attempts -eq 1 ] ; then
 echo "No more guesses. You are dead :(" The correct word was \"$correctWord\"
 #sets guess to correct word to restart loop
 userGuess="$correctWord"
 else
 echo "The letter \"$userGuess\" does not appear in the word."
 guessedLetters="$guessedLetters$userGuess"
 attempts=$(( $attempts - 1 ))
 fi
 done
 
 #asks user if they would like to play again
 echo ""
 echo -n "Would you like to play again? [yes/no]: "
 read userAnswer
 # if yes, the loop continues and repeats
 if [ $userAnswer = "yes" ] ; then 
 echo ""
 continue
 # if no, the loop will end, ending the game
 elif [ $userAnswer = "no" ] ; then
 echo ""
 echo "Thanks for playing!"
 exit 0
 fi
done
exit 0
